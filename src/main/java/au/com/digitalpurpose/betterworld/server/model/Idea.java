package au.com.digitalpurpose.betterworld.server.model;

import javax.persistence.*;

@Entity
public class Idea {

    @Id
    @SequenceGenerator(name= "IDEA_SEQ", sequenceName = "IDEA_SEQ_ID", initialValue=1, allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="IDEA_SEQ")
    private Long id;

    @Enumerated(EnumType.STRING)
    private Goal goal;

    private String title;
    private String author;
    private String description;
    private String thumbnailUrl;
    private Long likeCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public void incrementLikeCount() {
        likeCount++;
    }

    public Long getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Long likeCount) {
        this.likeCount = likeCount;
    }
}

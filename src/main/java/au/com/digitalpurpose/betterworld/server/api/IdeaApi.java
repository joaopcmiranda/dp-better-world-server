package au.com.digitalpurpose.betterworld.server.api;

import au.com.digitalpurpose.betterworld.server.model.Goal;
import au.com.digitalpurpose.betterworld.server.model.Idea;
import au.com.digitalpurpose.betterworld.server.service.IdeaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class IdeaApi {

    private final IdeaService ideaService;

    @Autowired
    public IdeaApi(IdeaService ideaService) {
        this.ideaService = ideaService;
    }

    @GetMapping("idea")
    public Iterable<Idea> findIdeas(
            @RequestParam(value = "searchTerm", required = false) String searchTerm,
            @RequestParam(value = "goal", required = false) Goal goal
    ) {
        return ideaService.findIdeas(searchTerm, goal);
    }

    @GetMapping("idea/{id}")
    public Idea getIdea(@PathVariable(value = "id") long id) {
        return ideaService.getIdea(id);
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public void addIdea(@RequestBody Idea idea) {
        ideaService.addIdea(idea);
    }

    @PostMapping( "like/{id}")
    public void like(@PathVariable(value = "id") long id) {
        ideaService.like(id);
    }
}

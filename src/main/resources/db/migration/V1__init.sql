CREATE TABLE idea
(
    id            INT AUTO_INCREMENT PRIMARY KEY,
    goal          VARCHAR NOT NULL,
    author        VARCHAR NOT NULL,
    title         VARCHAR NOT NULL,
    description   VARCHAR,
    like_count    INT DEFAULT 0,
    thumbnail_url VARCHAR
);

insert into idea (goal, title, author, description, thumbnail_url)
values ('sustainability', 'Clothing made from recycled material', 'Andrew',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://scx2.b-cdn.net/gfx/news/hires/2018/sustainable.jpg');

insert into idea (goal, title, author, description, thumbnail_url)
values ('education', 'Free, online school for everyone', 'Andrew',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://cdn-res.keymedia.com/cms/images/ca/126/0348_637326621445860317.jpg');

insert into idea (goal, title, author, description, thumbnail_url)
values ('climateAction', 'Insect farming as a food source', 'Andrew',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://i.pinimg.com/564x/95/05/d7/9505d74989aa7c2fc4b92674019fc080.jpg');

insert into idea (goal, title, author, description, thumbnail_url)
values ('climateAction', 'Ghost net removal program', 'Andrew',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://www.projectaware.org/sites/www.projectaware.org/files/styles/post/public/Scuba%20Fish%20Thailand%20%28C%29%20Liquid%20Lense.jpg');

insert into idea (goal, title, author, description, thumbnail_url)
values ('climateAction', 'Justice reinvestment community resilience program', 'Andrew',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'https://assets.change.org/photos/8/cj/tz/SAcjTzoknSiBqwy-400x400-noPad.jpg?1515940789');

CREATE SEQUENCE IDEA_SEQ_ID START WITH (select max(ID) + 1 from idea);